package parkinglot;

public class Car extends Vehicles{
	public Car() {
		spotNeeded = 3;
		type = VehicleType.CAR;
		
	}
	@Override
	public boolean canFitSpot(ParkingSpace space) {
		if(space.getSpotNumber() >= spotNeeded) return true;
		return false;
	}
}
