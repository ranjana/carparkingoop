package parkinglot;

public class Data {
	static boolean space[][];
	static boolean[][] createparkinglot(){
		space = new boolean[100][100];
		return space;
	}
	
	static ParkingDetail checkforAvailabiliityOfSpace(int spaceNeed) {
		space = new boolean[20][20];
		for(int i =0; i<20;i++) {
			for(int j =0; j<20;j++) {
				space[i][j] = true;
			 }
			}
		space[0][0] = false;
		int x[] =new int[2];
		boolean ishorizontal = true;
		ParkingDetail parkingDetail = new ParkingDetail(); 
		boolean hasSpace = false;
		for(int i =0; i<20-spaceNeed;i++) {
			for(int j =0; j<20-spaceNeed;j++) {
			if(space[i][j]) {
				for(int k =0;k<spaceNeed;k++) {
				 if(space[i][j+k]) {	 
					 hasSpace = true; 
				     }else {
					  hasSpace = false;
					  break;
				     }
				   }
				if(hasSpace) {
					x[0] = i;
					x[1] =j;
					parkingDetail.setCordinate(x);
					parkingDetail.setHorizontal(ishorizontal);
					return parkingDetail;
				}
			for(int k =0;k<spaceNeed;k++) {
				 if(space[i+k][j]) {	 
					 hasSpace = true;
					 ishorizontal = false;
				     }else {
					  hasSpace = false;
					  ishorizontal = true;
					  break;
				     }
				   }
		
			if(hasSpace) {
				x[0] = i;
				x[1] =j;
				parkingDetail.setCordinate(x);
				parkingDetail.setHorizontal(ishorizontal);
				return parkingDetail;

			}
			}
				
			}
		   }
		return parkingDetail;
	}
	
	public static boolean park( ParkingDetail parkingdetail,int k) {
		if(parkingdetail.isHorizontal()) {
			for(int i = parkingdetail.getCordinate()[1];i<k;i++ ) {
				space[parkingdetail.getCordinate()[0]][i] = false;
			}
		}else {
			for(int i = parkingdetail.getCordinate()[0];i<k;i++ ) {
				space[i][parkingdetail.getCordinate()[1]] = false;
			}
		}
		return false;
		
	}
	public static void printAllData() {
		for(int i =0; i<20;i++) {
			for(int j =0; j<20;j++) {
				System.out.print(space[i][j]);
			 }
			System.out.print("\n");
			}
	}

}