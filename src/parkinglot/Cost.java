package parkinglot;

public class Cost {
	private VehicleType type;
	private int timeInHr;
	private Level level;
	public VehicleType getType() {
		return type;
	}
	public void setType(VehicleType type) {
		this.type = type;
	}
	public int getTimeInHr() {
		return timeInHr;
	}
	public void setTimeInHr(int timeInHr) {
		this.timeInHr = timeInHr;
	}
	public Level getLevel() {
		return level;
	}
	public void setLevel(Level level) {
		this.level = level;
	}
	
}
