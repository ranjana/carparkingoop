package parkinglot;

public class ElectricCar extends Car{
	private int chargingTime;

	public int getChargingTime() {
		return chargingTime;
	}

	public void setChargingTime(int chargingTime) {
		this.chargingTime = chargingTime;
	}


}
