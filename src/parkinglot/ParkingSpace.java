package parkinglot;

public class ParkingSpace {
	private Vehicles vehicle; 
	private VehicleType type; 
	private int row;
	private int spotNumber; 
	private Level level;
	
	public ParkingSpace(Vehicles vehicle, VehicleType type, int row, int spotNumber, Level level) {
		super();
		this.vehicle = vehicle;
		this.type = type;
		this.row = row;
		this.spotNumber = spotNumber;
		this.level = level;
	}
	public Vehicles getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicles vehicle) {
		this.vehicle = vehicle;
	}
	public VehicleType getType() {
		return type;
	}
	public void setType(VehicleType type) {
		this.type = type;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getSpotNumber() {
		return spotNumber;
	}
	public void setSpotNumber(int spotNumber) {
		this.spotNumber = spotNumber;
	}
	public Level getLevel() {
		return level;
	}
	public void setLevel(Level level) {
		this.level = level;
	}


}
