package parkinglot;

public class Bike extends Vehicles{
	
	public Bike() {
		spotNeeded = 2;
		type = VehicleType.BIKE;
		
	}
	@Override
	public boolean canFitSpot(ParkingSpace space) {
		if(space.getSpotNumber() >= spotNeeded) return true;
		return false;
	}
}
