package parkinglot;

public class Truck extends Vehicles{
	
	public Truck() {
		spotNeeded = 5;
		type = VehicleType.TRUCK;
		
	}

	@Override
	public boolean canFitSpot(ParkingSpace space) {
		if(space.getSpotNumber() >= spotNeeded) return true;
		return false;
	}

}
