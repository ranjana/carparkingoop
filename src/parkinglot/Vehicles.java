package parkinglot;

public abstract class Vehicles {
private String numberPlat;
private String color;
protected VehicleType type;
protected int spotNeeded;


public String getNumberPlat() {
	return numberPlat;
}
public void setNumberPlat(String numberPlat) {
	this.numberPlat = numberPlat;
}
public String getColor() {
	return color;
}
public void setColor(String color) {
	this.color = color;
}
public VehicleType getType() {
	return type;
}
public void setType(VehicleType type) {
	this.type = type;
}

public abstract boolean canFitSpot(ParkingSpace space);
public int getSpotNeeded() {
	return spotNeeded;
}
public void setSpotNeeded(int spotNeeded) {
	this.spotNeeded = spotNeeded;
}

}
