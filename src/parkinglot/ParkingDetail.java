package parkinglot;

public class ParkingDetail {
	private int cordinate[];
	private boolean isHorizontal;
	public boolean isHorizontal() {
		return isHorizontal;
	}
	public void setHorizontal(boolean isHorizontal) {
		this.isHorizontal = isHorizontal;
	}
	public int[] getCordinate() {
		return cordinate;
	}
	public void setCordinate(int cordinate[]) {
		this.cordinate = cordinate;
	}

}
