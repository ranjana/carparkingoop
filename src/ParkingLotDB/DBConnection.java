package ParkingLotDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import parkinglot.ParkingSpace;

public class DBConnection {
	static Connection con = null;
	public static void createConnectionToDB() {
		String url = "jdbc:mysql://localhost:3306/user_Information?useSSL=false";
		String username = "root";
		String pass = "root";
		try {
			Class.forName("com.mysql.jdbc.Driver");
				con = DriverManager.getConnection(url,username,pass);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{ 
            System.out.println("finally block executed"); 
        }
		
	}
	public static void createAllTable() {
		Statement stmt = null;

	    try {
	      stmt = con.createStatement();

	      stmt.executeUpdate(DataBaseHelper.PARKINGSPSCE);

	      System.out.println("Table created");

	    } catch (SQLException e) {
	      e.printStackTrace();
	    } finally {
	      try {
	        // Close connection
	        if (stmt != null) {
	          stmt.close();
	        }
	        if (con != null) {
	          con.close();
	        }
	      } catch (Exception e) {
	        e.printStackTrace();
	      }
	    }
		
	}
	void insertIntoParkingSpaceTable(ParkingSpace space){
		String sql = "INSERT INTO "+DataBaseHelper.PARKINGSPSCE +" VALUES (?,?,?)"; 
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, space.getRow());
//			ps.setString(2,space.getType()));
//			ps.setString(3,space.surname);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
